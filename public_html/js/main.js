/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//$("#commentForm").validate();
jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Solo letras");

$(document).ready(function(){
    $('#commentForm').popover('show');


$("#commentForm").validate({
    rules:{
        nombres:{
            required: true,
            lettersonly:true
        },
        email:{
            required:true,
            email:true
        },
        dni:{
            required:true,
            number:true,
            minlength:8,
            maxlength:8
        },
        telefono:{
            required:true,
            number:true,
            minlength:7,
            maxlength:12
        }
    },
    messages:{
        nombres:{
            required:"Requiere un nombre",
            lettersonly:"Escriba solo letras"
        },
        email:{
            required:"Requiere un correo valido",
            email:"El correo tiene un formato: example@com.pe"
        },
        telefono:{
            required:"Requiere un numero telefonico",
            number:"Escriba solo numeros",
            maxlength:"Solo permite 12 digitos",
            minlength:"Escriba un telefono existente"
        },
        dni:{
            required:"Requiere un numero de DNI valido",
            number:"Escriba solo numeros",
            maxlength:"Solo permite 8 digitos",
            minlength:"Solo permite 8 digitos"
        }
    },
    /*errorElement: 'span',
        errorClass: 'error',
        validClass: 'success',
        highlight: function(element, errorClass) {
            $(element).popover('show');
        },
        unhighlight: function(element, errorClass) {
            $(element).popover('hide');
        },
        errorPlacement: function(err, element) {
            err.hide();
        }*/
    
});
});
/*$('#nombres').popover({
        placement: 'below',
        offset: 20,
        trigger: 'manual'
    });
    $('#email').popover({
        placement: 'below',
        offset: 20,
        trigger: 'manual'
    });
    $('#dni').popover({
        placement: 'below',
        offset: 20,
        trigger: 'manual'
    });*/

/*$("#commentForm").popover({
    content:'Escriba los datos correctos'
})*/
